# Kali-NetHunter-Uninstaller
Kali NetHunter Uninstaller is a simple flashable zip to wipe Whole Kali chroot and its apps and binaries from your system without setting up your android system and whole rom again.
It turns Your Kali Nethunter Phone into simple android device as it was  Before.

# DISCLAIMER
Before using it,read it carefully. This zip will work only if you have installed kali nethunter by flashing generic arm64 or armhf chroot.Otherwise if you have installed using magsik module,you have to remove them manually.
And I have tested it for A/B devices only since I dont have A device.But It will work as far i can tell.
and I am not responsible for bricked device,void warranty and thermonucler war.

# Features
1. Supports Both A and A/B devices
2. Supports Both Kali arm64 and Armhf chroot(full chroot .minimal support will be added soon.)
3. Covers all /system path
4. Time Saving tool(Takes 1-3 mintues)

# Limitation
1. Can't Replace the Kali Nethunter Boot animation( i will try to add support of a nice boot animation.but its not possible to get back the orginal one unless you add the bootanimation yourself)
2. Cant delete the wallpaper (Have to Delete Them Manually)
3. Minimal Chroot is not Supported(for Now)
4. Can't Uninstall apps from /data in A10 and A9 if they are installed manually

# How It works
1. Download the KaliNethunterUninstallerV1.zip from release.
2. Put it in internal storage.
3. Boot in Twrp or any other custom rom.Mount /system from mount option.(sometimes mounting system is not necessary)
4. Flash the Zip via Twrp.It will take 1-3 min.
5. Wipe Dalvik/Cache .
6. Reboot :)

# Issues
You tell me.I have found no issues .

# SCREENSHOT

![Screenshot_2020-12-04-12-55-27](/uploads/ca515a2d4f4be8e6f5ff5ddbb3fefbd4/Screenshot_2020-12-04-12-55-27.png)
# Author and Acknowledgement
Author : Mominul Islam Hemal (Telegram: @n00b_00)

Thanks to Team-420 for supporting.(Telegram: @team420nethunter)

Update-binary Script By osm0sis @ xda-developers
